﻿using PZ1.Model;
using PZ1.ViewModel;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ1
{
    public class MainWindowViewModel : BindableBase
    {
        private AlbumViewModel albumViewModel = new AlbumViewModel();
        private AccountViewModel accountViewModel = new AccountViewModel();
        private AddViewModel addViewModel = new AddViewModel();
        private HomeViewModel homeViewModel = new HomeViewModel();
        public MyICommand<string> NavCommand { get; private set; }
        private BindableBase currentViewModel;


        public MainWindowViewModel()
        {
            NavCommand = new MyICommand<string>(OnNav);
            CurrentViewModel = homeViewModel;
            HomeViewModel.LoggedIn += OnLogIn;
            HomeViewModel.Registered += OnRegister;
            AccountViewModel.Registered += OnLogIn;
            AddViewModel.Registered += OnRegister;
        }


        public BindableBase CurrentViewModel
        {
            get { return currentViewModel; }
            set
            {
                SetProperty(ref currentViewModel, value);
            }
        }

        private void OnNav(string destination)
        {
            switch (destination)
            {
                case "home":
                    CurrentViewModel = homeViewModel;
                    break;
                case "accountDetails":
                    CurrentViewModel = accountViewModel;
                    break;
                case "addImage":
                    CurrentViewModel = addViewModel;
                    break;
                case "myImages":
                    CurrentViewModel = albumViewModel;
                    break;
            }
        }

        private void OnLogIn(object sender, EventArgs e)
        {
            ((MainWindow)System.Windows.Application.Current.MainWindow).Menu.Visibility = System.Windows.Visibility.Visible;
            CurrentViewModel = albumViewModel;
        }

        private void OnRegister(object sender, EventArgs e)
        {
            ((MainWindow)System.Windows.Application.Current.MainWindow).Menu.Visibility = System.Windows.Visibility.Visible;
            CurrentViewModel = addViewModel;
        }


    }
}
