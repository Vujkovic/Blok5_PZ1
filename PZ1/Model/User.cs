﻿using PZ1.ViewModel;
using PZ1.XML;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace PZ1.Model
{
    public class User : ValidationBase
    {
        private string username;
        private string password;
        private List<Fotka> album = new List<Fotka>();

        

        public string Username
        {
            get { return username; }
            set
            {
                if (username != value)
                {
                    username = value;
                    OnPropertyChanged("Username");
                }
            }
        }

        public string Password
        {
            get { return password; }
            set
            {
                if (password != value)
                {
                    password = value;
                    OnPropertyChanged("Password");
                }
            }
        }

        public List<Fotka> Album
        {
            get { return album; }
            set
            {
                if (album != value)
                {
                    album = value;
                    OnPropertyChanged("Album");
                }
            }
        }

        protected override void ValidateSelf(string s)
        {
            bool postoji = false;
            User x = new User();
            foreach (User u in HomeViewModel.Users)
            {
                if (u.Username == this.username)
                {
                    postoji = true;
                    x = u;
                    break;
                }
            }
            if (s.Equals("r"))
            {
                if (!string.IsNullOrWhiteSpace(this.username))
                {
                    if(Regex.IsMatch(this.username, @"^\d"))
                    {
                        this.ValidationErrors["Username"] = "Username ne sme pocinjati sa cifrom.";
                    }
                    if (postoji)
                    {
                        this.ValidationErrors["Username"] = "Username postoji.";
                    }
                }
                else
                {
                    this.ValidationErrors["Username"] = "Username prazan.";
                }

                if (!string.IsNullOrWhiteSpace(this.password))
                {
                    if (this.password.Length < 6)
                    {
                        this.ValidationErrors["Password"] = "Password mora biti duzi od 6";
                    }
                }
                else
                {
                    this.ValidationErrors["Password"] = "Password prazan";
                }
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(this.username))
                {
                    if (!postoji)
                    {
                        this.ValidationErrors["Username"] = "Username ne postoji.";
                    }
                    if (Regex.IsMatch(this.username, @"^\d"))
                    {
                        this.ValidationErrors["Username"] = "Username ne sme pocinjati sa cifrom.";
                    }
                }
                else
                {
                    this.ValidationErrors["Username"] = "Username prazan.";
                }

                if (!string.IsNullOrWhiteSpace(this.password))
                {
                    if (this.password.Length < 6)
                    {
                        this.ValidationErrors["Password"] = "Password mora biti duzi od 6";
                       
                    }else if (this.password != x.password)
                    {
                        this.ValidationErrors["Password"] = "Password pogresan";
                    }
                }
                else
                {
                    this.ValidationErrors["Password"] = "Password prazan";
                }

            }
        }
    }
}
