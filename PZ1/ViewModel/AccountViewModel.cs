﻿using PZ1.Model;
using PZ1.XML;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ1.ViewModel
{
    public class AccountViewModel : BindableBase
    {
        public static event EventHandler Registered;

        public MyICommand SaveChangesCommand { get; set; }
        private ListFromXml usersList;
        public static User loggedInUser = new User();
        private string newUsername;
        private string newPassword;

        public string NewUsername
        {
            get { return newUsername; }
            set
            {
                newUsername = value;
                OnPropertyChanged("NewUsername");
            }
        }
        public string NewPassword
        {
            get { return newPassword; }
            set
            {
                newPassword = value;
                OnPropertyChanged("NewPassword");
            }
        }

        public User LoggedInUser
        {
            get { return loggedInUser; }
            set
            {
                loggedInUser = value;
                OnPropertyChanged("LoggedInUser");
            }
        }

        public AccountViewModel()
        {
            usersList = ListFromXml.getInstance();
            SaveChangesCommand = new MyICommand(SaveChanges);
        }

        private void SaveChanges()
        {
            bool postoji = false;
            usersList.ReadFromXml();
            HomeViewModel.Users = usersList.Users;
            string s = LoggedInUser.Username;
            LoggedInUser.Password = NewPassword;
            LoggedInUser.Username = NewUsername;
            HomeViewModel.currentUser = LoggedInUser;
            LoggedInUser.Validate("r");
            if (LoggedInUser.IsValid)
            {
                for(int i =0; i<HomeViewModel.Users.Count; i++)
                {
                    if (HomeViewModel.Users[i].Username == s)
                    {
                        HomeViewModel.Users[i] = LoggedInUser;
                        usersList.SaveToXml();
                        Registered(this, EventArgs.Empty);
                        break;
                    }
                }
                
            }
        }
    }
}
