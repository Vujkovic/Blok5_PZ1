﻿using PZ1.Model;
using PZ1.XML;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ1.ViewModel
{
    public class AddViewModel : BindableBase
    {
        public static event EventHandler Registered;
        public Fotka pic = new Fotka();
        public MyICommand FindImageCommand { get; set; }
        public MyICommand SavePictureCommand { get; set; }
        public string slik;
        private string titleText;
        private string descriptionText;
        private ListFromXml usersList;

        public Fotka Pic
        {
            get { return pic; }
            set
            {
                pic = value;
                OnPropertyChanged("Pic");
            }
        }

        public string Slik
        {
            get { return slik; }
            set
            {
                if (slik != value)
                {
                    slik = value;
                    OnPropertyChanged("Slik");
                }
            }
        }

        public string TitleText
        {
            get { return titleText; }
            set
            {
                if (titleText != value)
                {
                    titleText = value;
                    OnPropertyChanged("TitleText");
                }
            }
        }

        public string DescriptionText
        {
            get { return descriptionText; }
            set
            {
                if (descriptionText != value)
                {
                    descriptionText = value;
                    OnPropertyChanged("DescriptionText");
                }
            }
        }

        public AddViewModel()
        {
            usersList = ListFromXml.getInstance();
            FindImageCommand = new MyICommand(OnFind);
            SavePictureCommand = new MyICommand(OnSave);
        }

        private void OnFind()
        {

            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.Filter = dlg.Filter = "All Files|*.*";
            Nullable<bool> result = dlg.ShowDialog();
            if (result == true)
            {
                string filename = dlg.FileName;
                //Image.Source = new ImageSourceConverter().ConvertFromString(filename) as ImageSource;
                Slik = filename;
            }
        }

        private void OnSave()
        {
            User pom = new User();
            usersList.ReadFromXml();
            HomeViewModel.Users = usersList.Users;
            foreach (User u in HomeViewModel.Users)
            {
                if (u.Username == HomeViewModel.currentUser.Username)
                {
                    pom = u;
                }
            }

            HomeViewModel.Users.Remove(pom);
            pic.Path = slik;
            pic.Title = TitleText;
            pic.Description = DescriptionText;
            pic.Date = DateTime.Now;
            pic.Validate("p");
            if (pic.IsValid)
            { 
                pom.Album.Add(pic);
                HomeViewModel.Users.Add(pom);
                usersList.SaveToXml();
                TitleText = "";
                DescriptionText = "";
                Slik = "";
                Registered(this, EventArgs.Empty);
            }
        }
    }
}
