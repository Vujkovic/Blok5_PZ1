﻿using PZ1.Model;
using PZ1.XML;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ1.ViewModel
{
    public class AlbumViewModel : BindableBase
    {
        private string printLoggedinUser;
        private ListFromXml usersList;

        private List<Fotka> pictures;

        public AlbumViewModel()
        {
            pictures = new List<Fotka>();
            usersList = ListFromXml.getInstance();
            usersList.ReadFromXml();
            HomeViewModel.Users = usersList.Users;
            if (HomeViewModel.Users == null)
            {

            }
            else
            {
                foreach (User u in HomeViewModel.Users)
                {
                    if (u.Username == HomeViewModel.currentUser.Username)
                    {
                        Pictures = u.Album;
                    }
                }
            }


        }

        public List<Fotka> Pictures
        {
            get { return pictures; }
            set
            {
                if (pictures != value)
                {
                    pictures = value;
                    OnPropertyChanged("Pictures");
                }
            }
        }
        public string PrintLoggedinUser
        {
            get { return HomeViewModel.currentUser.Username; } //return "Hello!";
            set
            {
                if (HomeViewModel.currentUser.Username != value)
                {
                    printLoggedinUser = value;
                    OnPropertyChanged("PrintLoggedinUser");
                }
            }
        }
    }
}
