﻿using PZ1.Model;
using PZ1.XML;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ1.ViewModel
{
    public class HomeViewModel : BindableBase
    {
        public static event EventHandler LoggedIn;
        public static event EventHandler Registered;

        public MyICommand RegisterCommand { get; set; }
        public MyICommand LogInCommand { get; set; }

        public static User currentUser = new User();
        private ListFromXml usersList;


        public static List<User> Users { get; set; }
        
        public User CurentUser
        {
            get { return currentUser; }
            set
            {
                currentUser = value;
                OnPropertyChanged("CurrentUser");
            }
        }


        public HomeViewModel()
        {
            Users = new List<User>();
            usersList = ListFromXml.getInstance();
            LogInCommand = new MyICommand(LogInExecute, LogInCanExecute);
            RegisterCommand = new MyICommand(RegisterExecute);
        }

        private void LogInExecute()
        {
            bool postoji = false;
            usersList.ReadFromXml();
            Users = usersList.Users;
            CurentUser.Validate("l");
            if (CurentUser.IsValid)
            {
                foreach (User u in Users)
                {
                    if (u.Username == currentUser.Username)
                    {
                        postoji = true;
                        if(u.Password == currentUser.Password)
                        {
                            AccountViewModel.loggedInUser = u;
                            LoggedIn(this, EventArgs.Empty);
                            break;
                        }
                    }
                }
            }
        }

        private bool LogInCanExecute()
        {
            return true;
        }

        private bool RegisterCanExecute()
        {
            bool postoji = false;
            foreach (User u in Users)
            {
                if (u.Username == currentUser.Username)
                {
                    postoji = true;
                    break;
                }
            }
            return !postoji;
        }

        private void RegisterExecute()
        {
            bool postoji = false;
            usersList.ReadFromXml();
            Users = usersList.Users;
            CurentUser.Validate("r");
            if (CurentUser.IsValid)
            {
                foreach (User u in Users)
                {
                    if (u.Username == currentUser.Username)
                    {
                        postoji = true;
                        break;
                    }
                }
                if (!postoji)
                {
                    User Userc = new User();
                    Userc.Username = CurentUser.Username;
                    Userc.Password = CurentUser.Password;
                    
                    Users.Add(Userc);
                
                        
                    usersList.SaveToXml();
                    AccountViewModel.loggedInUser = Userc;
                    Registered(this, EventArgs.Empty);
                }
            }
        }
    }
}
